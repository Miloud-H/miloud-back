<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class MessageDataPersister implements DataPersisterInterface
{
    private EntityManagerInterface $entityManager;
    private ?Request $request;
    private MailerInterface $mailer;
    private LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $request,
        MailerInterface $mailer,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->request = $request->getCurrentRequest();
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function supports($data): bool
    {
        return $data instanceof Message;
    }

    /**
     * @param Message $data
     */
    public function persist($data)
    {
        $data->setIp($this->request->getClientIp());
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        $email = new TemplatedEmail();
        $email->from($data->getEmail())
            ->to('haboudoumiloud@gmail.com')
            ->subject($data->getSubject())
            ->htmlTemplate('mail.html.twig')
            ->context([
                'data' => $data
            ]);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error('Erreur lors de l\'envoi d\'un mail', ['exception' => $e]);
        }
    }

    public function remove($data)
    {
        // TODO: Implement remove() method.
    }
}
