<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route(name="cv_download", path="/cv")
     */
    public function downloadCV(){
        return $this->file('../public/files/CV_HABOUDOU_Miloud.pdf');
    }
}
